# QGI Analysis Template 

The below README and repo has been adapted from PBPython (thank you, Chris Moffit) in order to benefit QGI's workflow and processes.  Please read the below to understand how to use this template.

Most common uses:  
1) Copying this template into your own repo at the beginning of a project to set consistent directories and workflows within ipynbs  
2) Altering this repo's source template to better everyone's experience when accessing it via cookiecutter   
- To do so, simply copy this repo, only edit the ipynb file (don't edit the {{cookiecutter}} element, and make a pull request  
- eg: you'd like to add in consistent data viz libraries and methods  


#  Cookiecutter Template: Repeatable Data Analysis with Notebooks 

A [cookiecutter template](https://cookiecutter.readthedocs.io/en/1.7.2/) for simple data analysis.

Full details and walk-through over at [**Practical Business Python:  Building a Repeatable Data Analysis Process with Jupyter Notebooks** ](http://pbpython.com/notebook-process.html) on the background and how to use this cookiecutter template.

## Folder structure

This template will jumpstart your data science projects with the following predictable organizational file structure:

```bash
.
├── 1-Data prep and analysis.ipynb  # Data prep and analysis notebook
├── data               # Categorized data files
│   ├── external       # External data files  
│   ├── interim        # Working folder
│   ├── processed      # Cleaned and ready to use
│   └── raw            # Unmodified originals
└── reports            # Final reports
```

## Installation

To use Cookiecutter, you must have it installed along with Python 3. Once you have Python installed, the recommended way to install Cookiecutter is as follows. Install to the current user's folder, upgrade if available:

```bash
$ pip3 install -U --user cookiecutter
```

## Usage

Then in the folder you want to *contain* the project you're starting, run the template as follows, answering the questions as relevant to your project:

```bash
$ cookiecutter https://brianpreisler@bitbucket.org/brianpreisler/qgi-analysis-template.git     
project_name [project_name]: 
directory_name [data_journalism_project]: 
description [More background on the project]: 
```

Now, in this example, we'll have a folder `` with the structure described above ready to get to work!
